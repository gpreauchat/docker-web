#!/bin/bash

IFS='/' read -ra PARTS <<< "$PWD" # one-line solution
DRUSH_PATH=''

# Search any drush version in the project (Drupal 8)
for i in ${PARTS[@]}; do
  DRUSH_PATH="$DRUSH_PATH/$i"
  if [ -e "${DRUSH_PATH}/vendor/drush/drush/drush" ]
    then
      ${DRUSH_PATH}/vendor/drush/drush/drush "$@"
      exit
  fi
done

# If no drush found in the project we used the  default version
drush "$@"

