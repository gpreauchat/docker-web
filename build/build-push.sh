#!/bin/bash

# authenticate with the Docker Hub registry
docker login --username gpreauchat

# build the Docker image (this will use the Dockerfile in the root of the repo)
# push the new Docker images to the Docker registry
docker build --build-arg APACHE_VERSION="2.4" -t gpreauchat/drupal-apache:2.4 ./apache/
docker push gpreauchat/drupal-apache:2.4
docker build build --build-arg PHP_VERSION="5.6" -t gpreauchat/drupal-php:5.6 ./php/ -t gpreauchat/drupal-php:5.6 ./php/
docker push gpreauchat/drupal-php:5.6
docker build --build-arg PHP_VERSION="7.1" -t gpreauchat/drupal-php:7.1 ./php/
docker push gpreauchat/drupal-php:7.1
docker build --build-arg PHP_VERSION="7.2" -t gpreauchat/drupal-php:7.2 ./php/
docker push gpreauchat/drupal-php:7.2
docker build --build-arg PHP_VERSION="7.3" -t gpreauchat/drupal-php:7.3 ./php/
docker push gpreauchat/drupal-php:7.3
docker build --build-arg MARIADB_VERSION="10.3" -t gpreauchat/drupal-mariadb:10.3 ./mariadb/
docker push gpreauchat/drupal-mariadb:10.3