# Site PROJECT_NAME

## Installation ##

### Prérequis ###

Afin d'avoir tous les outils nécessaire pour executer cette application correctement, vous devez tout d'abord installer **docker** et **docker-compose** sur votre machine si ce n'est pas déjà fait.

- [Guide d'installation de docker](https://docs.docker.com/install/)
- [Guide d'installation de docker-compose](https://docs.docker.com/compose/install/)

### Ajout des hosts ###

Sur votre machine, il faut ajouter le domaine **"example.local"** dans le fichier */etc/hosts* (linux) ou */Windows/System32/drivers/etc/hosts* (Windows) afin d'accèder correctement à notre application

### Création de l'image docker ###

Afin de créer votre image docker vous devez executer la commande suivante à la racine de votre projet.

``` bash
docker-compose build
```

> Cette étape est à réaliser à chaque fois que le fichier **docker/docker-compose** ou **docker/Dockerfile** est modifié

### Lancement du docker ###

Pour lancer votre application vous devez exécuter la commande suivante à la racine de votre projet.

``` bash
docker-compose up -d
```

L'environnement docker de ce projet est composé d'un serveur apache avec PHP, un serveur MySQL, d'une application PHPMyAdmin

> Si vous rencontrez une erreur lié à des ports déjà utilisés, vous pouvez modifier les ports configurés dans le fichier **.env**

### Initialisation de la base de données ###

Pour initialiser la BdD, il faut tout d'abord créer une base de données appelée **PROJECT_NAME** (Pour accéder à la base de données cf partie *Accès à la base de données*)

Ensuite, il faut importer la base via le dump SQL

``` bash
// Listez les différents container docker lancés sur votre machine
// Puis copiez le nom du container mysql utilisé par l'application
// ex: drupal_example_database
docker ps
// Lancez la commande dans le docker
docker exec -i [nom_du_container] mysql -h mariadb -u root example < docker/example.sql
```

### Mise à jour des packages composer ###

Cette application utilise des dépendances composer, afin de les installer ou de les mettre à jours connectez-vous tout d'abord à votre container via cette commande.

``` bash
// Listez les différents container docker lancés sur votre machine
// Puis copiez le nom du container php utilisé par l'application
// ex: drupal_example
docker ps
//Connectez vous au docker
docker exec -it [nom_du_container] bash
```

Ensuite lancez la commande suivante:
``` bash
composer install
```

### Mise à jour des packages NPM ###

Cette application utilise des dépendances NPM, afin de les installer ou de les mettres à jours connectez-vous tout d'abord à votre container via cette commande.

``` bash
// Listez les différents container docker lancés sur votre machine
// Puis copiez le nom du container php utilisé par l'application
// ex: drupal_example
docker ps
//Connectez vous au docker
docker exec -it [nom_du_container] bash
```

Ensuite lancez la commande suivante:
``` bash
npm install
```
